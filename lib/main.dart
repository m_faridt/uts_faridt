import 'package:flutter/material.dart';
import './output.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'UTS PEMROGRAMAN MOBILE SMT6',
      home: Scaffold(
        backgroundColor: Colors.grey[100],
        appBar: AppBar(
          backgroundColor: Colors.black,
          leading: new Icon(Icons.home),
          title: Text('UTS PEMROGRAMAN MOBILE SMT6'),
          actions: <Widget>[
          new Icon(Icons.search),
        ],
        ),
        body: Output(),
      ),
    );
  }
}